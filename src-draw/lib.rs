
// Importa todas as funções que refletem a API basica do JS e do Browser:
use wasm_bindgen::prelude::*;
use wasm_bindgen::Clamped;
use web_sys::{ ImageData };
use std::iter;

struct Painter {
    width: u32,
    height: u32,
    pixels: Vec<f32>
}

impl Painter {
    fn new(img_data: ImageData) -> Painter {
        Painter {
            width: img_data.width(),
            height: img_data.height(),
            pixels: img_data.data().to_vec().iter()
                            .map(|&b| b as f32 / 255.0).collect()
        }
    }

    fn xy_to_pos(&self, x: i32, y: i32) -> usize {
        let ux = i32::clamp(x, 0, (self.width-1) as i32) as u32;
        let uy = i32::clamp(y, 0, (self.height-1) as i32) as u32;
        4 as usize * (uy * self.width + ux) as usize
    }

    fn get_px(&self, x: i32, y: i32) -> (f32, f32, f32, f32) {
        let pos = self.xy_to_pos(x, y);
        (
            self.pixels[pos+0],
            self.pixels[pos+1],
            self.pixels[pos+2],
            self.pixels[pos+3]
        )
    }

    fn mk_blur_convolution_matrix(&self, radius: f32) -> Vec<Vec<f32>> {
        let conv_radius = radius.ceil() as u32;
        let conv_width = conv_radius as usize * 2 + 1;
        let mut conv_tot: f32 = 0.0;
        let mut matrix: Vec<Vec<f32>> = iter::repeat_with(||
                iter::repeat_with(|| 0.0f32).take(conv_width).collect()
            ).take(conv_width).collect();
        for y in -(conv_radius as i32)..(conv_radius as i32) {
            for x in -(conv_radius as i32)..(conv_radius as i32) {
                let dist = ((x*x + y*y) as f32).sqrt() / radius;
                let val: f32 = if dist < 1.0 {
                    1.0 - (dist * dist)
                } else {
                    0.0
                };
                conv_tot += val;
                matrix[(y+conv_radius as i32) as usize][(x+conv_radius as i32) as usize] = val;
            }
        }
        for y in 0..(conv_radius*2+1) as usize {
            for x in 0..(conv_radius*2+1) as usize {
                matrix[y][x] /= conv_tot;
            }
        }
        matrix
    }

    fn blur(&mut self, radius: f32) {
        let mut pix_copy = self.pixels.clone();
        let matrix = self.mk_blur_convolution_matrix(radius);
        for y in 0..self.height as i32 {
            for x in 0..self.width as i32 {
                let (r,g,b,a) = self.convolute_px(&matrix, x, y);
                let pos = self.xy_to_pos(x, y);
                pix_copy[pos+0] = r;
                pix_copy[pos+1] = g;
                pix_copy[pos+2] = b;
                pix_copy[pos+3] = a;
            }
        }
        self.pixels = pix_copy;
    }

    fn convolute_px(&self, matrix: &Vec<Vec<f32>>, cx: i32, cy: i32) -> (f32, f32, f32, f32) {
        let radius = (matrix.len() as i32 - 1) / 2;
        let mut tot_r = 0f32;
        let mut tot_g = 0f32;
        let mut tot_b = 0f32;
        let mut tot_a = 0f32;
        let mut tot_mult = 0f32;
        let mut tot_mult_a = 0f32;
        for y in -radius..radius {
            for x in -radius..radius {
                let mult = matrix[(y+radius) as usize][(x+radius) as usize];
                let (r,g,b,a) = self.get_px(x+cx as i32, y+cy as i32);
                tot_mult += mult;
                tot_mult_a += mult * a;
                tot_r += r as f32 * mult * a;
                tot_g += g as f32 * mult * a;
                tot_b += b as f32 * mult * a;
                tot_a += a as f32 * mult;
            }
        }
        let presence_adjust = tot_mult / tot_mult_a;
        (
            tot_r * presence_adjust,
            tot_g * presence_adjust,
            tot_b * presence_adjust,
            tot_a
        )
    }

    fn to_image_data(&self) -> Result<ImageData, JsValue> {
        ImageData::new_with_u8_clamped_array_and_sh(
            Clamped::<&[u8]>(
                &self.pixels.iter()
                .map(|&f| (f * 255.0).round() as u8)
                .collect::<Vec<u8>>()
            ),
            self.width,
            self.height
        )
    }
}

#[wasm_bindgen]
pub fn draw(img_data: ImageData) -> Result<ImageData, JsValue> {
    let mut painter = Painter::new(img_data);
    painter.blur(12.0);
    painter.to_image_data()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn some_test() {
        // teste it
    }
}
