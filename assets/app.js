function showSrc(boxId, html, rust) {
    const divSrc = document.createElement('div')
    divSrc.className = 'example-src'
    document.getElementById(boxId).appendChild(divSrc)

    const opts = document.createElement('p')
    divSrc.appendChild(opts)

    const btnHTML = document.createElement('button')
    opts.appendChild(btnHTML)
    btnHTML.innerText = 'HTML'

    const btnRust = document.createElement('button')
    opts.appendChild(btnRust)
    btnRust.innerText = 'Rust'

    const codeEl = document.createElement('pre')
    divSrc.appendChild(codeEl)
    codeEl.append(rust)

    btnHTML.onclick = ()=> updateCode(codeEl, html)
    btnRust.onclick = ()=> updateCode(codeEl, rust)
}

function updateCode(codeEl, code) {
  while (codeEl.firstChild) codeEl.firstChild.remove()
  codeEl.append(code)
}
