
// Importa todas as funções que refletem a API basica do JS e do Browser:
use wasm_bindgen::prelude::*;
use web_sys;

#[wasm_bindgen]
extern {
    fn alert(s: &str);
}

#[wasm_bindgen]
pub fn greet() {
    alert("Olá, WASM!");
}

#[wasm_bindgen(start)]
pub fn run() -> Result<(), JsValue> {
    // Use `web_sys`'s global `window` function to get a handle on the global
    // window object.
    let window = web_sys::window().expect("no global `window` exists");
    let document = window.document().expect("should have a document on window");
    //let body = document.body().expect("document should have a body");
    let root = document.query_selector("#hello-wasm").unwrap().expect("#hello-wasm el not found");

    // Manufacture the element we're gonna append
    let val = document.create_element("p")?;
    val.set_text_content(Some("Olá do Rust! :-)"));

    root.append_child(&val)?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_greets() {
        greet();
    }
}
