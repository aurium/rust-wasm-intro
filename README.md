Rust do Zero ao WebAssembly
===========================

Exemplos introdutórios para trabalhar com Rust e WebAssembly.
Veja os exemplos: https://aurium.gitlab.io/rust-wasm-intro

Cada diretório `src-*` nesse projeto é um exemplo independente que pode ser lido de forma isolada.

Instalando toolchain
--------------------
```
$ curl --proto '=https' --tlsv1.3 https://sh.rustup.rs -sSf | sh
...
1) Proceed with installation (default)
...
```

Setup current shell env:
```
$ source "$HOME/.cargo/env"
```

Wasm workflow tool:
```
$ curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh
```

Estrutura Básica de um Projeto Rust
-----------------------------------
```
Proj-Root
    ├── Cargo.toml  # Descreve o projeto e suas dependências, como o `package.json`
    └── src
        └── my-wasm-program.rs  # Deve-se apontar no `Cargo.toml`, ou usar `lib.rs`
```
Mas os exemplos aqui seguem uma estrutura mais simples, sem subdiretórios.
Para saber mais, leia capitulo "[The Manifest Format](https://doc.rust-lang.org/cargo/reference/manifest.html)", no "The Cargo Book".

Compilando WASM e módulo JS
---------------------------

Esse é a forma geral de construir um projeto:
```
wasm-pack build --target web
```
Você pode executar isso dentro de cada diretório `src-*`.

Produz os diretórios `target` (ignore-o) e `pkg`, onde você encontra um módulo JS com o nome definido no `Cargo.toml`, que pode ser incorporado e usado por uma aplicação web.

Se você quiser construir esse projeto, execute `./build.sh` e carregue o `./dist/index.html` no seu navegador, a partir de um serviço http local.


Teste
-----

```
$ cargo test src/lib-hello-wasm.rs
```
or
```
$ cargo watch -x 'test src/lib-hello-wasm.rs'
```

(`watch` depende de `cargo install cargo-watch`)


Coisas Potencialmente Úteis
---------------------------

### Experimente online!

https://play.rust-lang.org

### Modos de execução

* `cargo play <file.rs>`  
  Compila, informa mais que o `rustc` e executa um arquivo independente de projeto. E suporta o parâmetro `--test`  
  `$ cargo install cargo-play`
* `cargo run`  
  Compila, informa e executa o projeto corrente.  
* `$ cargo watch -x run`  
  Recompila e executa automaticamente a cada mudança dos fontes.  
  `$ cargo install cargo-watch`

### Libs & Frameworks

* **[Seed](https://seed-rs.org)** Framework elm-like para WASM WebApps.
* **[winit](https://docs.rs/winit)** a cross-platform window creation and event loop management library. ([src](https://github.com/rust-windowing/winit))
* **[Druid](https://github.com/linebender/druid)** A data-first Rust-native UI toolkit.
* **[egui](https://www.egui.rs)** An easy-to-use GUI in pure Rust.

### QA

* **[Miri](https://github.com/rust-lang/miri)** mid-level intermediate representation interpreter.  
  É experimental, mas pode capturar erros que o compilador não é capaz.
* **[Clippy](https://doc.rust-lang.org/nightly/clippy)**, lints to catch common mistakes and improve your Rust code.

### Documentação de Código

O rust doc aceita comentários em markdown para enriquecer a documentação automática.

```
$ cargo doc
```
Cria toda a documentção do projeto na pasta `./target/doc`

### Extenções VSCode

* rust-rls: Rust Language Service, provê docs, autocompliete e +.
* rust-analyzer: autocomplete e outras coisas
* Better TOML: identifica os *.toml
* crates: valida dependências no toml

### Aulas, Docs, Ajuda...

* [Doc oficial](https://www.rust-lang.org/learn)
* [Livro: The Rust Programming Language](https://doc.rust-lang.org/book/)
* [Livro: Rust and WebAsm](https://rustwasm.github.io/docs/book/)
* [Livro: The `wasm-bindgen` Guide](https://rustwasm.github.io/wasm-bindgen/)
* [Video-aulas: Rust-Começar](https://www.youtube.com/playlist?list=PLjSf4DcGBdiHC1rf9rXR9orU3wvGjgtpm)
* [Video-aulas: The Rust Lang Book](https://www.youtube.com/playlist?list=PLai5B987bZ9CoVR-QEIN9foz4QCJ0H2Y8) (baseadas no livro)
* [structs, traits, and zero-cost abstractions by Tim McLean](https://www.youtube.com/watch?v=Sn3JklPAVLk) (traits são mais econômicos que generics, mas gera overhead)
