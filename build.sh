#!/bin/sh -e

test -e dist && rm -r dist
mkdir dist

n='
'

CONTENT_LINE=$(grep -h -n %CONTENT% assets/index.html | cut -d: -f1)

head -n $(($CONTENT_LINE-1)) assets/index.html > dist/index.html

ls -1 assets/* | grep -v /index.html | xargs -I'{}' cp '{}' dist/

file_to_js_data() {
  echo -n "\`$(cat "$1" | sed -r 's/`/\\`/g; s/([<$])/\1`+`/g')\`"
}

for SRC in src-*; do
    echo ">> Building $SRC"
    test -e pkg && rm -r pkg

    wasm-pack --verbose build --target web --out-dir $PWD/pkg $SRC

    mv pkg/*.wasm pkg/*.js dist/

    echo "$n<div id=\"$SRC\" class=\"example-box\">
    <div class=\"example-result\">
    <h2> $(echo $SRC | sed 's/src-//; s/-/ /g') </h2>
    <article>" >> dist/index.html
    cat $SRC/page-slice.html >> dist/index.html
    echo "$n<article>
    </div>
    </div>
    <script> showSrc(
      '$SRC',
      $(file_to_js_data $SRC/page-slice.html),
      $(file_to_js_data $SRC/*.rs)
    ) </script>
    " >> dist/index.html

    rm -r pkg
done

tail -n +$(($CONTENT_LINE+1)) assets/index.html >> dist/index.html
